def verifyUser(username):
    restricted_types = ('&', ' ', '$', '\'', '/', '*', '@', '<', '>', '(', ')', '[', ']', '{', '}')
    # Length test
    if len(username) < 3 or len(username) > 15:
        return False
    # Only-digit test
    elif username.isdigit():
        return False
    # Special chars test
    elif [ele for ele in restricted_types if(ele in username)]:
        return False
    # First letter is lower test
    elif username[0].islower():
        return False
    else:
        return True


def main():
    verifyUser(input("Saisissez votre username : "))

if __name__ == "__main__":
    main()
