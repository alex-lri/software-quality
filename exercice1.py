# Exercice 1:
# Faire un programme qui permet de récupérer sur l’entrée utilisateur une phrase.
# Puis en afficher le type.
# Ensuite il faut que vous transformez cette phrase en list de mot.
# Voici le résultat attendu:

# "je suis une legende"
# [“je”, “suis”, “une”, “legende”]


phrase = input('Entrez votre phrase : ')
# chaine de caractère entrer par l'utiliserateur
print(type(phrase))
# type() permet de connaitre quel type de variable j'ai en résultat
print('Votre phrase est :', str.split(phrase))
# split() permet de convertir une chaîne de caractère en liste
