# Exercice 2:
# Créer une calculatrice simple.
# Il faut demander à l’utilisateur un premier nombre.
# Puis un opérateur
# Puis un deuxième nombre.
# Ensuite il faut écrire le résultat dans un fichier dont l'utilisateur choisit le nom.
# (Attention aux types des variables)


def main():

    nombre1 = int(input('Entrez un premier nombre: '))
    # on utilise int() pour recevoir uniquement un nombre entier
    operateur = input('Choisir un opérateur ( - | + | / | * ):  ')
    # on demande d'entrer un opérateur parmis ceux proposés a notre utilisateur
    nombre2 = int(input('Entrez un deuxième nombre: '))
    fichierName = input('Entez un nom de fichier pour stocker le resultat :')

    # traitement calculatrice : 4 operateurs possibles

    # traitement soustraction
    if operateur == "-":
        resultat = (nombre1 - nombre2)
    # traitement addition
    elif operateur == "+":
        resultat = (nombre1 + nombre2)
    # traitement division
    elif operateur == "/":
        resultat = (nombre1 / nombre2)
    # traitement multiplication
    elif operateur == "*":
        resultat = (nombre1 * nombre2)

    # ouvrir un fichier nommé par l'utilisateur
    # ajout de l'extension .txt
    # mode write
    fichier = open(fichierName + ".txt", "w")
    # Écrit dans le fichier et conversion de notre resultat en string
    fichier.write(str(resultat))
    # Ferme le fichier
    fichier.close()


if __name__ == "__main__":
    main()
