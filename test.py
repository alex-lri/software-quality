import unittest
from exercices_test2 import username


class TestUserName(unittest.TestCase):
    # Test on a valid username
    def test_valid_username(self):
        self.assertTrue(username.verifyUser('Azerty154'))

    # Only numbers in username
    def test_only_numbers_username(self):
        self.assertFalse(username.verifyUser('15482565'))

    # No special chars in submitted username
    def test_special_chars_username(self):
        self.assertFalse(username.verifyUser('-/*--@_  &>><'))
        self.assertFalse(username.verifyUser('A545e(é-54'))

    # Minimal length of submitted username should be at least 3
    def test_minimal_username_length(self):
        self.assertFalse(username.verifyUser('Aa'))

    # Maximum length of submitted username should not be more than 15
    def test_maximum_username_length(self):
        self.assertFalse(username.verifyUser('Gzeahrghezjgreazjhgreazjgrezjgrhjezgrhjezgrhjzeahgj'))

    # First char of the submitted username should be uppercase
    def test_uppercase_first_char(self):
        self.assertFalse(username.verifyUser('aaaaaa'))
        self.assertTrue(username.verifyUser('Aaaaaa'))

if __name__ == '__main__':
    unittest.main()
