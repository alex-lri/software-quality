import unittest
import calc


class TestCalc(unittest.TestCase):
    def test_addition(self):
        self.assertEqual(calc.operation('+', 1, 1), 2)

    def test_sousrtaction(self):
        self.assertEqual(calc.operation('-', 2, 1), 1)

    def test_multiplication(self):
        self.assertEqual(calc.operation('*', 2, 3), 6)

    def test_sousrtaction(self):
        self.assertEqual(calc.operation('/', 10, 5), 2)

    def test_wrong_operator(self):
        with self.assertRaises(TypeError):
            calc.operation('$', 1, 1)

    def test_division_zero(self):
        self.assertEqual(calc.operation('/', 1, 0), 0)


if __name__ == '__main__':
    unittest.main()
